<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'product_name'); ?>
		<?php echo $form->textField($model,'product_name',array('size'=>60,'maxlength'=>555)); ?>
		<?php echo $form->error($model,'product_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php 
		echo $form->dropDownList($model,'category_id', CHtml::listData(Category::model()->findAll(), 'id', 'category_name'), array('empty'=>'--please select--'));
		?>
		
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>
	
	<?php if(!empty($model->image)){?>
	 
	<div class="row">		
		<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/products/'.$model->image.'','image',array("style"=>"width:93px;" ));?>
	</div>
	<?php }?>
	
	<div class="row">	
	<?php echo $form->labelEx($model, 'image');?>
	<?php echo $form->fileField($model, 'image',array('size'=>60,'maxlength'=>555));?>
	<?php echo $form->error($model, 'image');?>
	</div>
	
	<div class="row">
	<?php echo $form->labelEx($model, 'description');?>
	<?php echo $form->textArea($model, 'description',array('cols'=>50,'rows'=>5,'maxlength'=>555));?>
	<?php echo $form->error($model, 'description');?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->